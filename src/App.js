import React, {Component} from 'react';
import {connect} from 'react-redux';
import ReactTablePage from "./ui/ReactTablePage/ReactTablePage";
import {fakeData} from "./fakeData";
import {
  SET_CURRENT_PAGE,
  SET_ITEMS,
  SET_ITEMS_PER_PAGE,
  SET_LOADING,
  SET_OPEN_DROPDOWN
} from "./state/paginator/action";

const titles = [
  "userId",
  "id",
  "title",
  "body"
];

class App extends Component {
  constructor(props){
    super(props);
    this.getData = this.getData.bind(this);
    this.getData()
  }

  getData(){
    const {setLoading, setItems} = this.props;
    setLoading(true);
    setItems(fakeData);
    setLoading(false)
  }

  render() {
    const {
      loading,
      items,
      currentPage,
      setCurrentPage,
      itemsPerPage,
      setItemsPerPage,
      handleToggle,
      dropDownOpen
    } = this.props;

    if (loading) return <h2>Loading...</h2>;

    return (
      <div>
        <ReactTablePage
          items={items}
          loading={loading}
          titles={titles}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          itemsPerPage={itemsPerPage}
          setItemsPerPage={setItemsPerPage}
          handleToggle={handleToggle}
          dropDownOpen={dropDownOpen}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  items: state.reducer.items,
  loading: state.reducer.loading,
  currentPage: state.reducer.currentPage,
  itemsPerPage: state.reducer.itemsPerPage,
  dropDownOpen: state.reducer.dropDownOpen
});

const mapDispatchToProps = dispatch => ({
  setItems: (items) => dispatch ({type: SET_ITEMS, items: items}),
  setItemsPerPage: (number) => dispatch ({type: SET_ITEMS_PER_PAGE, itemsPerPage: number}),
  setCurrentPage: (page) => dispatch ({type: SET_CURRENT_PAGE, currentPage: page}),
  handleToggle: () => dispatch ({type: SET_OPEN_DROPDOWN}),
  setLoading: (loading) => dispatch ({type: SET_LOADING, loading: loading})
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
