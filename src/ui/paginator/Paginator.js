import React from 'react';
import {
  ButtonDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Pagination,
  PaginationItem,
  PaginationLink
} from "reactstrap";

const Paginator = ({ itemsPerPage, totalItems,
                     dropDownOpen, handleToggle,
                     paginate, setItemsPerPage,
                     classname }) => {

  const pageNumbers = [];
  for(let i = 1; i<= Math.ceil(totalItems / itemsPerPage); i++) {
    pageNumbers.push(i)
  }

  return (
    <div className={classname}>
      <ButtonDropdown isOpen={dropDownOpen} toggle={handleToggle} direction="down">
        <DropdownToggle caret>
          {itemsPerPage}
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem onClick={() => setItemsPerPage(10)}>10</DropdownItem>
          <DropdownItem onClick={() => setItemsPerPage(20)}>20</DropdownItem>
          <DropdownItem onClick={() => setItemsPerPage(50)}>50</DropdownItem>
        </DropdownMenu>
      </ButtonDropdown>
      <Pagination aria-label="Page navigation">
        {pageNumbers.map(number => {
            return (
                <PaginationItem key={number}>
                  <PaginationLink onClick={() => paginate(number)} href="!#">
                    {number}
                  </PaginationLink>
                </PaginationItem>
            )
          })}
      </Pagination>
    </div>
  )
};

export default Paginator;
