import React from 'react';
import PropTypes from 'prop-types';
import Paginator from "../../ui/paginator/Paginator";
import MyTable from "../table/MyTable";



const ReactTablePage = ({
  items,
  currentPage,
  itemsPerPage,
  setItemsPerPage,
  setCurrentPage,
  handleToggle,
  dropDownOpen,
  titles }) => {

    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentItems = items.slice(indexOfFirstItem, indexOfLastItem);

    const paginate = pageNumber => setCurrentPage(pageNumber);

    return (
      <div className="ReactTablePage">
        <h4>Paginator</h4>
        <MyTable
          tableHeader={titles}
          items={currentItems}
          striped={true}
          bordered={true}
        />
        <Paginator
          itemsPerPage={itemsPerPage}
          setItemsPerPage={setItemsPerPage}
          totalItems={items.length}
          paginate={paginate}
          dropDownOpen={dropDownOpen}
          handleToggle={handleToggle}
        />
      </div>
    );
  };

ReactTablePage.propTypes = {
  items: PropTypes.array.isRequired,
  currentPage: PropTypes.number.isRequired,
  itemsPerPage: PropTypes.number.isRequired,
  setItemsPerPage: PropTypes.func.isRequired,
  setCurrentPage: PropTypes.func.isRequired,
  handleToggle: PropTypes.func.isRequired,
  dropDownOpen: PropTypes.bool.isRequired,
  titles: PropTypes.array.isRequired
};

export default ReactTablePage;
