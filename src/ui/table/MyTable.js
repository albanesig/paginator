import React from 'react';
import {Table} from "reactstrap";

const MyTable = ({bordered, striped, items, tableHeader, classname}) => {
  return (
    <Table
      className={classname}
      bordered={bordered}
      striped={striped}>
      <thead>
      <tr>
        {tableHeader && tableHeader.map((title, i) => (
          <th key={i}>{title}</th>
        ))}
      </tr>
      </thead>
      <tbody>
      {items && items.map((item, i) => {
          return <tr key={i}>
            {Object.values(item).map(value => (
              <td key={i}>{value}</td>
            ))}
          </tr>
        })}
      </tbody>
    </Table>
  )
};

export default MyTable;