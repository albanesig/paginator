import { combineReducers } from 'redux';
import {SET_CURRENT_PAGE, SET_ITEMS, SET_ITEMS_PER_PAGE, SET_LOADING, SET_OPEN_DROPDOWN} from "./paginator/action";

const defaultState = {
  items: [],
  loading: false,
  currentPage: 1,
  itemsPerPage: 10,
  dropDownOpen: false
};

const reducer = (state = defaultState, action = {}) => {
  switch (action.type) {
    case SET_ITEMS:
      return {
        ...state,
        items: action.items
      };
    case SET_ITEMS_PER_PAGE:
      return {
        ...state,
        itemsPerPage: action.itemsPerPage
      };
    case SET_LOADING:
      return {
        ...state,
        loading: action.loading
      };
    case SET_CURRENT_PAGE:
      return {
        ...state,
        currentPage: action.currentPage
      };
    case SET_OPEN_DROPDOWN:
      return {
        ...state,
        dropDownOpen: !state.dropDownOpen
      };
    default:
      return state
  }
};


export default combineReducers({
  reducer
});
